# Travel eGuitar Project

This repo contains everything needed to build a travel eguitar:
- design files
- cnc files
- electronics specs
- code to run basic amplifier functions on a small board built into the eguitar

This project is work in progress and not (yet) finished. I built the first guitar end of 2017 and it requires some necessary changes, i.e. a metal rod to stabilize the too soft neck.

