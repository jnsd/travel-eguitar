# Body Design & Building

## Sources

[Making a Through-Neck Bass Guitar From Scratch](https://www.instructables.com/id/Making-a-Through-Neck-Bass-From-Scratch/)

[How to make a low-cost guitar amp with Linux](https://opensource.com/article/17/8/linux-guitar-amp)

[High-resolution DAC and ADC for RPi](https://www.hifiberry.com/docs/data-sheets/datasheet-dac-adc/)