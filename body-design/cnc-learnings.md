# CNC Learnings

## CAD

- Create one special object/component that allows to cut out the basic shape before turning and milling 3D slopes - as these do not touch the bottom, cutting the piece might not be possible
- Use an outer alignment box to align the cut part after flipping it upside down

## CAM

- Define WCS at alignment box corner
- **IMPORTANT**: Set clearance height offset from retract height to 0 mm for all paths
- ​

### 3D Pocket Clearing

- Used to roughly remove the material down to the 3D shape
- Settings
  - Select the model surfaces
  - Select stock contours to restrict the path to the selected area
  - It might be necessary to make hovering body parts touch the bottom stock surface to get the stock contours right 
  - Machine boundary = Bounding box
  - Tool containment = Tool center on boundary
  - Stock to leave: both set to 0.5 mm

### 3D Parallel

- Used to finish a roughly cleared 3D shaped pocket
- Settings
  - Machining boundary = Silhouette
  - Tool containment = Tool center in boundary
  - Pass direction = 90° to go up/down
  - Stepover approx. 60-80% of cutter diameter

## CNC

1. Milli the top side, i.e. cutting the outer shape - if the bottom has 3D slopes, use a separate body that resembles the outer cut shape
2. Detach the part and alignment frame 
3. Remove all tabs between alignment frame and stock material
4. Re-align the alignment frame to one corner of the stock material
5. Re-align flipped+moved object to origin of CNC machine coordinate system and gcode:
   1. Option 1: Move the cutter by cutter diameter towards aligned corner and reset x/y=0/0
   2. Option 2: Move the body in CAD according to the (new) position in the stock corner and create gcode based on this WCS origin - keep the CNC machine coordinate system origin