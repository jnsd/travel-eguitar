#!/bin/bash

# Start this script with 'sudo ./setup.sh'
# The below setup expects a full raspbian being flashed on the sd card

apt-get update
apt-get upgrade -y
apt-get install -y puredata
apt-get install alsa-base alsa-utils
apt-get install tightvncserver

# this requires the external usb sound card to be installed
echo -e 'pcm.card1 {\n  type hw\n  card 1\n}\nctl.card1 {\n  type hw\n  card 1\n}\n\npcm.!default card1' >> /etc/asound.conf
