(1001)
(TRAVEL EGUITAR TUNING MOUNTS)
(T3  D=3. CR=0. - ZMIN=0. - FLAT END MILL)
G0 G90 G94 G17
G21

(2D POCKET7)
M5
M9
T3
M3 S16000
G54
M9
Z20.
G0 X75.7 Y53.5
G1 Z18. F1000.
Z13. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y53.303 I-0.2 J0.
G1 X75.933 Y53.697
G3 X75.7 Y53.5 I-0.033 J-0.197
G1 Z11. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y53.303 I-0.2 J0.
G1 X75.933 Y53.697
G3 X75.7 Y53.5 I-0.033 J-0.197
G1 Z9. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y53.303 I-0.2 J0.
G1 X75.933 Y53.697
G3 X75.7 Y53.5 I-0.033 J-0.197
G1 Z7. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y53.303 I-0.2 J0.
G1 X75.933 Y53.697
G3 X75.7 Y53.5 I-0.033 J-0.197
G1 Z5. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y53.303 I-0.2 J0.
G1 X75.933 Y53.697
G3 X75.7 Y53.5 I-0.033 J-0.197
G1 Z3. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y53.303 I-0.2 J0.
G1 X75.933 Y53.697
G3 X75.7 Y53.5 I-0.033 J-0.197
G1 Z1. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y53.303 I-0.2 J0.
G1 X75.933 Y53.697
G3 X75.7 Y53.5 I-0.033 J-0.197
G1 Z0. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
G1 X77.98 Z0.057
G0 Z20.
X107.45
G1 Z18. F500.
Z13.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y53.303 I-0.2 J0.
G1 X107.683 Y53.697
G3 X107.45 Y53.5 I-0.033 J-0.197
G1 Z11. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y53.303 I-0.2 J0.
G1 X107.683 Y53.697
G3 X107.45 Y53.5 I-0.033 J-0.197
G1 Z9. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y53.303 I-0.2 J0.
G1 X107.683 Y53.697
G3 X107.45 Y53.5 I-0.033 J-0.197
G1 Z7. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y53.303 I-0.2 J0.
G1 X107.683 Y53.697
G3 X107.45 Y53.5 I-0.033 J-0.197
G1 Z5. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y53.303 I-0.2 J0.
G1 X107.683 Y53.697
G3 X107.45 Y53.5 I-0.033 J-0.197
G1 Z3. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y53.303 I-0.2 J0.
G1 X107.683 Y53.697
G3 X107.45 Y53.5 I-0.033 J-0.197
G1 Z1. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y53.303 I-0.2 J0.
G1 X107.683 Y53.697
G3 X107.45 Y53.5 I-0.033 J-0.197
G1 Z0. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
G1 X109.73 Z0.057
G0 Z20.
X107.45 Y16.
G1 Z18. F500.
Z13.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y15.803 I-0.2 J0.
G1 X107.683 Y16.197
G3 X107.45 Y16. I-0.033 J-0.197
G1 Z11. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y15.803 I-0.2 J0.
G1 X107.683 Y16.197
G3 X107.45 Y16. I-0.033 J-0.197
G1 Z9. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y15.803 I-0.2 J0.
G1 X107.683 Y16.197
G3 X107.45 Y16. I-0.033 J-0.197
G1 Z7. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y15.803 I-0.2 J0.
G1 X107.683 Y16.197
G3 X107.45 Y16. I-0.033 J-0.197
G1 Z5. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y15.803 I-0.2 J0.
G1 X107.683 Y16.197
G3 X107.45 Y16. I-0.033 J-0.197
G1 Z3. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y15.803 I-0.2 J0.
G1 X107.683 Y16.197
G3 X107.45 Y16. I-0.033 J-0.197
G1 Z1. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
X110.067 Y15.803 I-0.2 J0.
G1 X107.683 Y16.197
G3 X107.45 Y16. I-0.033 J-0.197
G1 Z0. F500.
G2 X106.25 I-0.6 J0. F1000.
X110.3 I2.025 J0.
X103.4 I-3.45 J0.
X110.3 I3.45 J0.
G1 X109.73 Z0.057
G0 Z20.
X75.7
G1 Z18. F500.
Z13.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y15.803 I-0.2 J0.
G1 X75.933 Y16.197
G3 X75.7 Y16. I-0.033 J-0.197
G1 Z11. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y15.803 I-0.2 J0.
G1 X75.933 Y16.197
G3 X75.7 Y16. I-0.033 J-0.197
G1 Z9. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y15.803 I-0.2 J0.
G1 X75.933 Y16.197
G3 X75.7 Y16. I-0.033 J-0.197
G1 Z7. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y15.803 I-0.2 J0.
G1 X75.933 Y16.197
G3 X75.7 Y16. I-0.033 J-0.197
G1 Z5. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y15.803 I-0.2 J0.
G1 X75.933 Y16.197
G3 X75.7 Y16. I-0.033 J-0.197
G1 Z3. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y15.803 I-0.2 J0.
G1 X75.933 Y16.197
G3 X75.7 Y16. I-0.033 J-0.197
G1 Z1. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
X78.317 Y15.803 I-0.2 J0.
G1 X75.933 Y16.197
G3 X75.7 Y16. I-0.033 J-0.197
G1 Z0. F500.
G2 X74.5 I-0.6 J0. F1000.
X78.55 I2.025 J0.
X71.65 I-3.45 J0.
X78.55 I3.45 J0.
G1 X77.98 Z0.057
G0 Z20.
X43.95
G1 Z18. F500.
Z13.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y15.803 I-0.2 J0.
G1 X44.183 Y16.197
G3 X43.95 Y16. I-0.033 J-0.197
G1 Z11. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y15.803 I-0.2 J0.
G1 X44.183 Y16.197
G3 X43.95 Y16. I-0.033 J-0.197
G1 Z9. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y15.803 I-0.2 J0.
G1 X44.183 Y16.197
G3 X43.95 Y16. I-0.033 J-0.197
G1 Z7. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y15.803 I-0.2 J0.
G1 X44.183 Y16.197
G3 X43.95 Y16. I-0.033 J-0.197
G1 Z5. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y15.803 I-0.2 J0.
G1 X44.183 Y16.197
G3 X43.95 Y16. I-0.033 J-0.197
G1 Z3. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y15.803 I-0.2 J0.
G1 X44.183 Y16.197
G3 X43.95 Y16. I-0.033 J-0.197
G1 Z1. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y15.803 I-0.2 J0.
G1 X44.183 Y16.197
G3 X43.95 Y16. I-0.033 J-0.197
G1 Z0. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
G1 X46.23 Z0.057
G0 Z20.
X43.95 Y53.5
G1 Z18. F500.
Z13.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y53.303 I-0.2 J0.
G1 X44.183 Y53.697
G3 X43.95 Y53.5 I-0.033 J-0.197
G1 Z11. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y53.303 I-0.2 J0.
G1 X44.183 Y53.697
G3 X43.95 Y53.5 I-0.033 J-0.197
G1 Z9. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y53.303 I-0.2 J0.
G1 X44.183 Y53.697
G3 X43.95 Y53.5 I-0.033 J-0.197
G1 Z7. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y53.303 I-0.2 J0.
G1 X44.183 Y53.697
G3 X43.95 Y53.5 I-0.033 J-0.197
G1 Z5. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y53.303 I-0.2 J0.
G1 X44.183 Y53.697
G3 X43.95 Y53.5 I-0.033 J-0.197
G1 Z3. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y53.303 I-0.2 J0.
G1 X44.183 Y53.697
G3 X43.95 Y53.5 I-0.033 J-0.197
G1 Z1. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
X46.567 Y53.303 I-0.2 J0.
G1 X44.183 Y53.697
G3 X43.95 Y53.5 I-0.033 J-0.197
G1 Z0. F500.
G2 X42.75 I-0.6 J0. F1000.
X46.8 I2.025 J0.
X39.9 I-3.45 J0.
X46.8 I3.45 J0.
G1 X46.23 Z0.057
G0 Z20.

(2D CONTOUR5)
M9
G0 X24.685 Y-1.5
G1 Z20. F1000.
Z18.
Z17.5
X67.5 Z13. F750.
X135. F1000.
G3 X136.5 Y0. I0. J1.5
G1 Y30.
G3 X135. Y31.5 I-1.5 J0.
G1 X0.
G3 X-1.5 Y30. I0. J-1.5
G1 Y0.
G3 X0. Y-1.5 I1.5 J0.
G1 X67.5
X101.442 Z11. F750.
X135. F1000.
G3 X136.5 Y0. I0. J1.5
G1 Y30.
G3 X135. Y31.5 I-1.5 J0.
G1 X0.
G3 X-1.5 Y30. I0. J-1.5
G1 Y0.
G3 X0. Y-1.5 I1.5 J0.
G1 X101.442
X135. Z9.023 F750.
G3 X135.381 Y-1.451 Z9. I0. J1.5
X136.5 Y0. I-0.381 J1.451 F1000.
G1 Y30.
G3 X135. Y31.5 I-1.5 J0.
G1 X0.
G3 X-1.5 Y30. I0. J-1.5
G1 Y0.
G3 X0. Y-1.5 I1.5 J0.
G1 X135.
G3 X135.381 Y-1.451 I0. J1.5
X136.5 Y0. Z8.884 I-0.381 J1.451 F750.
G1 Y30. Z7.116
G3 X135.381 Y31.451 Z7. I-1.5 J0.
X135. Y31.5 I-0.381 J-1.451 F1000.
G1 X0.
G3 X-1.5 Y30. I0. J-1.5
G1 Y0.
G3 X0. Y-1.5 I1.5 J0.
G1 X135.
G3 X136.5 Y0. I0. J1.5
G1 Y30.
G3 X135.381 Y31.451 I-1.5 J0.
X135. Y31.5 Z6.977 I-0.381 J-1.451 F750.
G1 X101.442 Z5.
X0. F1000.
G3 X-1.5 Y30. I0. J-1.5
G1 Y0.
G3 X0. Y-1.5 I1.5 J0.
G1 X135.
G3 X136.5 Y0. I0. J1.5
G1 Y30.
G3 X135. Y31.5 I-1.5 J0.
G1 X101.442
X67.5 Z3. F750.
X0. F1000.
G3 X-1.5 Y30. I0. J-1.5
G1 Y0.
G3 X0. Y-1.5 I1.5 J0.
G1 X135.
G3 X136.5 Y0. I0. J1.5
G1 Y30.
G3 X135. Y31.5 I-1.5 J0.
G1 X67.5
X33.558 Z1. F750.
X0. F1000.
G3 X-1.5 Y30. I0. J-1.5
G1 Y19.41
Z2.
Y11.41
Z1. F500.
Y0. F1000.
G3 X0. Y-1.5 I1.5 J0.
G1 X38.167
Z2.
X46.167
Z1. F500.
X135. F1000.
G3 X136.5 Y0. I0. J1.5
G1 Y10.992
Z2.
Y18.992
Z1. F500.
Y30. F1000.
G3 X135. Y31.5 I-1.5 J0.
G1 X116.969
Z2.
X108.969
Z1. F500.
X33.558 F1000.
X16.586 Z0. F750.
X0. F1000.
G3 X-1.5 Y30. I0. J-1.5
G1 Y19.41
Z2.
Y11.41
Z0. F500.
Y0. F1000.
G3 X0. Y-1.5 I1.5 J0.
G1 X38.167
Z2.
X46.167
Z0. F500.
X135. F1000.
G3 X136.5 Y0. I0. J1.5
G1 Y10.992
Z2.
Y18.992
Z0. F500.
Y30. F1000.
G3 X135. Y31.5 I-1.5 J0.
G1 X116.969
Z2.
X108.969
Z0. F500.
X16.586 F1000.
G0 Z20.
X110.315 Y69.
G1 Z18. F1000.
Z17.5
X67.5 Z13. F750.
X0. F1000.
G3 X-1.5 Y67.5 I0. J-1.5
G1 Y37.5
G3 X0. Y36. I1.5 J0.
G1 X135.
G3 X136.5 Y37.5 I0. J1.5
G1 Y67.5
G3 X135. Y69. I-1.5 J0.
G1 X67.5
X33.558 Z11. F750.
X0. F1000.
G3 X-1.5 Y67.5 I0. J-1.5
G1 Y37.5
G3 X0. Y36. I1.5 J0.
G1 X135.
G3 X136.5 Y37.5 I0. J1.5
G1 Y67.5
G3 X135. Y69. I-1.5 J0.
G1 X33.558
X0. Z9.023 F750.
G3 X-0.381 Y68.951 Z9. I0. J-1.5
X-1.5 Y67.5 I0.381 J-1.451 F1000.
G1 Y37.5
G3 X0. Y36. I1.5 J0.
G1 X135.
G3 X136.5 Y37.5 I0. J1.5
G1 Y67.5
G3 X135. Y69. I-1.5 J0.
G1 X0.
G3 X-0.381 Y68.951 I0. J-1.5
X-1.5 Y67.5 Z8.884 I0.381 J-1.451 F750.
G1 Y37.5 Z7.116
G3 X-0.381 Y36.049 Z7. I1.5 J0.
X0. Y36. I0.381 J1.451 F1000.
G1 X135.
G3 X136.5 Y37.5 I0. J1.5
G1 Y67.5
G3 X135. Y69. I-1.5 J0.
G1 X0.
G3 X-1.5 Y67.5 I0. J-1.5
G1 Y37.5
G3 X-0.381 Y36.049 I1.5 J0.
X0. Y36. Z6.977 I0.381 J1.451 F750.
G1 X33.558 Z5.
X135. F1000.
G3 X136.5 Y37.5 I0. J1.5
G1 Y67.5
G3 X135. Y69. I-1.5 J0.
G1 X0.
G3 X-1.5 Y67.5 I0. J-1.5
G1 Y37.5
G3 X0. Y36. I1.5 J0.
G1 X33.558
X67.5 Z3. F750.
X135. F1000.
G3 X136.5 Y37.5 I0. J1.5
G1 Y67.5
G3 X135. Y69. I-1.5 J0.
G1 X0.
G3 X-1.5 Y67.5 I0. J-1.5
G1 Y37.5
G3 X0. Y36. I1.5 J0.
G1 X67.5
X101.442 Z1. F750.
X135. F1000.
G3 X136.5 Y37.5 I0. J1.5
G1 Y49.983
Z2.
Y57.983
Z1. F500.
Y67.5 F1000.
G3 X135. Y69. I-1.5 J0.
G1 X117.538
Z2.
X109.538
Z1. F500.
X0. F1000.
G3 X-1.5 Y67.5 I0. J-1.5
G1 Y56.87
Z2.
Y48.87
Z1. F500.
Y37.5 F1000.
G3 X0. Y36. I1.5 J0.
G1 X38.238
Z2.
X46.238
Z1. F500.
X101.442 F1000.
X118.414 Z0. F750.
X135. F1000.
G3 X136.5 Y37.5 I0. J1.5
G1 Y49.983
Z2.
Y57.983
Z0. F500.
Y67.5 F1000.
G3 X135. Y69. I-1.5 J0.
G1 X117.538
Z2.
X109.538
Z0. F500.
X0. F1000.
G3 X-1.5 Y67.5 I0. J-1.5
G1 Y56.87
Z2.
Y48.87
Z0. F500.
Y37.5 F1000.
G3 X0. Y36. I1.5 J0.
G1 X38.238
Z2.
X46.238
Z0. F500.
X118.414 F1000.
G0 Z20.

M9
M30
